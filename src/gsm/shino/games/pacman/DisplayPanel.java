package gsm.shino.games.pacman;

import gsm.shino.games.pacman.actor.*;
import gsm.shino.games.pacman.audio.Audio;
import gsm.shino.games.pacman.map.Cell;
import gsm.shino.games.pacman.map.Map;
import gsm.shino.games.pacman.map.MapLevel1;
import gsm.shino.games.pacman.map.MapLevel2;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Timer;

public class DisplayPanel extends JPanel implements ActionListener, KeyListener {

    public static final int GAME_WIDTH = ((Cell.CELL_WIDTH + Cell.WALL_WIDTH)*Map.COLUMNS + Cell.WALL_WIDTH);
    public static final int GAME_HEIGHT = ((Cell.CELL_WIDTH + Cell.WALL_WIDTH)*9 + Cell.WALL_WIDTH);
    public static final int DISPLAY_WIDTH = GAME_WIDTH;
    public static final int DISPLAY_HEIGHT = GAME_HEIGHT + 100;
    public static final int REPAINT_DELAY = 10;
    public static final int START_MOVING_DELAY = 2000;
    private final Timer repaintTimer;
    private final Timer blinkingMessageTimer = new Timer(400, new Blinking());;
    private Color blinkingColor = Color.WHITE;
    private final Map[] maps = new Map[2];
    private final Pacman pacman;
    private final List<Ghost> ghosts = new ArrayList<Ghost>();
    private boolean gameFinished = false;
    private String finishMessage;
    private String userPromptMessage;
    private Actor winner;
    private int gameTime = 0;
    private boolean actorsMoving = false;
    private boolean gameFinishAudioPlayed = false;
    private boolean goingToNextLevel = true;
    private Color[] ghostColors = new Color[]{Color.PINK, Color.BLUE, Color.RED, Color.GREEN};

    public DisplayPanel() {
        setPreferredSize(new Dimension(DISPLAY_WIDTH, DISPLAY_HEIGHT));
        repaintTimer = new Timer(REPAINT_DELAY, this);
        repaintTimer.start();
        addKeyListener(this);
        setFocusable(true);
        maps[0] = new MapLevel1();
        maps[1] = new MapLevel2();
        pacman = new Pacman(this, maps[Game.LEVEL-1], 6 - Game.LEVEL);
        int ghostSpeed = 7 - Game.LEVEL;
        for(Color ghostColor : ghostColors) {
            ghosts.add(new Ghost(this, pacman, maps[Game.LEVEL-1], ghostColor, ghostSpeed));
        }
    }

    @Override
    public void paint(Graphics graphics) {
        maps[Game.LEVEL-1].draw(graphics);
        pacman.draw(graphics);
        for(Ghost ghost : ghosts) {
            ghost.draw(graphics);
        }
        showScoreAndLife(graphics);
        if(gameFinished) {
            pacman.setStepDistance(0);
            printFinishMessage(graphics);
        }
        showStatus(graphics);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        gameTime += REPAINT_DELAY;
        repaint();
    }

    /**
     * Show words when the game starts and control the starting time of all actors
     * between 0 to 1100 milliseconds: "Ready"
     * between 0 to 2200 milliseconds: "Set"
     *  between 0 to 3200 milliseconds: "Go!"
     *  after 3200 milliseconds: actors are able to move
     * @param graphics
     */
    private void showStatus(Graphics graphics) {
        if(gameTime < 1100) {
            drawStatus(graphics, "READY");
        } else if(gameTime < 2200) {
            drawStatus(graphics, "SET");
        } else if(gameTime < 3200) {
            drawStatus(graphics, "GO!");
        } else {
            startMoving();
        }
    }

    /**
     * Is used in showStatus method to set up the color, font, font size and position of the status
     * @param graphics
     * @param status is a string to show
     */
    private void drawStatus(Graphics graphics, String status) {
        graphics.setColor(Color.YELLOW);
        Font stringFont = new Font("Whiteboard", Font.BOLD, 38);
        graphics.setFont(stringFont);
        drawCenteredString(graphics, status, -48);
    }

    /**
     * Lets all the actors enable to start moving
     */
    private void startMoving() {
        actorsMoving = true;
        for(Ghost ghost : ghosts) {
            ghost.startMoving();
        }
        pacman.startMoving();
    }
    /**
     * Shows score and Pacman's life at the top of the panel
     * @param graphics
     */
    public void showScoreAndLife(Graphics graphics) {
        graphics.setColor(Color.WHITE);
        Font stringFont = new Font( "ArialBlack", Font.PLAIN, 18 );
        graphics.setFont(stringFont);
        graphics.drawString("Level: " + Game.LEVEL + "  Lives: " + Game.PACMAN_LIVES + "  Score: " + Game.SCORE, Cell.WALL_WIDTH, Map.STATUS_PANEL_HEIGHT - 10);
    }

    /**
     * Is used when either Pacman ate all the food on the map
     * or a ghost hit Pacman when Pacman does not have any lives left
     * @param success true if Pacman ate all the food
     *                false if a ghost hit Pacman when Pacman does not have any lives left
     * @param actor the actor who won
     *              If success is true the actor is Pacman,
     *              if success is false the actor is a ghost which hit Pacman at last.
     */
    public void finishGame(boolean success, Actor actor) {
        winner = actor;
        gameFinished = true;
        blinkingMessageTimer.start();
        if(success) {
            if(!gameFinishAudioPlayed) {
                Audio.WON.play();
                gameFinishAudioPlayed = true;
            }
            finishMessage = "Congratulations";
            if(Game.LEVEL >= 2) {
                userPromptMessage = "Press Enter To Start New Game";
                goingToNextLevel = false;
            } else {
                userPromptMessage = "Press Enter To Start Next Level";
                goingToNextLevel = true;
            }

        } else {
            finishMessage = "Game Over";
            if(!gameFinishAudioPlayed) {
                Audio.GAME_OVER.play();
                gameFinishAudioPlayed = true;
                userPromptMessage = "Press Enter To Try Again!";
            }
            goingToNextLevel = false;
        }
    }

    /**
     * Prints finish message.
     * It include message window, final message, winner actor, final score and user prompt message.
     * @param graphics
     */
    private void printFinishMessage(Graphics graphics) {
        drawMessageWindow(graphics);
        drawFinishMessage(graphics);
        drawWinner(graphics);
        drawFinalScore(graphics);
        drawUserPromptMessage(graphics);
    }

    /**
     * Draws message window. The size is 400 width and 200 height.
     * It shows at the middle of jPanel.
     * @param graphics
     */
    private void drawMessageWindow(Graphics graphics) {
        int halfWidth = 200;
        int halfHeight = 100;
        Graphics2D graphics2D = (Graphics2D) graphics;
        graphics2D.setColor(Color.BLACK);
        graphics2D.setStroke((new BasicStroke(10)));
        graphics2D.fillRect(DISPLAY_WIDTH / 2 - halfWidth, DISPLAY_HEIGHT / 2 - halfHeight, halfWidth * 2, halfHeight * 2);
        graphics2D.setColor(Color.WHITE);
        graphics2D.drawRect(DISPLAY_WIDTH / 2 - halfWidth, DISPLAY_HEIGHT / 2 - halfHeight, halfWidth * 2, halfHeight * 2);
    }

    /**
     * Draws finish message
     * When Pacman win the game, finish message is "Congratulation"
     * and when Ghosts win, finish message is "Game over".
     * It is shown on 45 pixels above from the middle of the window.
     * @param graphics
     */
    private void drawFinishMessage(Graphics graphics) {
        Font font = new Font("SansSerif", Font.PLAIN, 50);
        graphics.setFont(font);
        drawCenteredString(graphics, finishMessage, -45);
    }

    /**
     * Draws who win the game.
     * When Pacman win, the winner is Pacman,
     * and when Ghosts win, the winner is a ghost which hit Pacman at the end.
     * It is shown on at the middle of the window.
     * @param graphics
     */
    private void drawWinner(Graphics graphics) {
        winner.setStepDistance(0);
        winner.setX(DISPLAY_WIDTH / 2 - Actor.DIAMETER / 2);
        winner.setY(DISPLAY_HEIGHT / 2);
        winner.draw(graphics);
    }

    /**
     * Draws the final score which the player got.
     * It is shown on 70 pixels below from the middle of the window.
     * @param graphics
     */
    private void drawFinalScore(Graphics graphics) {
        graphics.setColor(Color.YELLOW);
        Font scoreFont = new Font("ArialBlock", Font.PLAIN, 30);
        graphics.setFont(scoreFont);
        drawCenteredString(graphics, "Your score is: " + Game.SCORE, 70);
    }

    /**
     * Prompts the player to press enter key to play the game again
     * Blinking timer to blink the words is used to distinguish this message from others
     * It is shown on at the bottom of the window.
     * @param graphics
     */
    private void drawUserPromptMessage(Graphics graphics) {
        graphics.setColor(blinkingColor);
        Font pressEnterFont = new Font("ArialBlock", Font.PLAIN, 20);
        graphics.setFont(pressEnterFont);
        drawCenteredString(graphics, userPromptMessage, 230);
    }

    /**
     * Shows the letter at the middle of the panel
     * @param graphics
     * @param message string what to show
     * @param row integer to adjust the y position to show the message
     */
    private void drawCenteredString(Graphics graphics, String message, int row) {
        FontMetrics fontMetrics = graphics.getFontMetrics();
        int x = (DISPLAY_WIDTH - fontMetrics.stringWidth(message)) / 2;
        int y = (fontMetrics.getAscent() + (DISPLAY_HEIGHT - (fontMetrics.getAscent() + fontMetrics.getDescent())) / 2) + row;
        graphics.drawString(message, x, y);
    }

    /**
     * Enables the player to restart the game again with pressing enter key.
     * This can be used only after the game is finished.
     * @param e
     */
    @Override
    public void keyPressed(KeyEvent e) {
        if(gameFinished && e.getKeyCode() == KeyEvent.VK_ENTER) {
            Game.restartGame();
        }
    }

    public List<Ghost> getGhosts() {
        return ghosts;
    }
    public boolean isGameFinished() {
        return gameFinished;
    }
    public boolean isGoingToNextLevel() {
        return goingToNextLevel;
    }

    @Override
    public void keyTyped(KeyEvent e) {}
    @Override
    public void keyReleased(KeyEvent e) {}

    /**
     * Inner class which is used to blink letters
     */
    private class Blinking implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(blinkingColor == Color.WHITE) {
                blinkingColor = Color.BLACK;
            } else {
                blinkingColor = Color.WHITE;
            }
        }
    }
}