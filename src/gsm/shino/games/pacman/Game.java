package gsm.shino.games.pacman;

import gsm.shino.games.pacman.audio.Audio;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;

public class Game {
    private static JFrame jFrame = new JFrame();
    private static DisplayPanel displayPanel;
    public static int LEVEL =  1;
    public static int PACMAN_LIVES = 2;
    public static int SCORE = 0;

    /**
     * main method of the project
     * @param args
     */
    public static void main(String[] args) {
        Audio.CHARGE.play();
        displayPanel = new DisplayPanel();
        jFrame.setResizable(false); 
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.add(displayPanel);
        centerFrame(jFrame);
        jFrame.setVisible(true);
        jFrame.pack();
    }

    /**
     * Shows the jFrame at the middle of the screen.
     * @param frame
     */
    private static void centerFrame(JFrame frame) {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - DisplayPanel.DISPLAY_WIDTH) / 2);
        int y = (int) ((dimension.getHeight() - DisplayPanel.DISPLAY_HEIGHT) / 2);
        frame.setLocation(x, y);
    }

    /**
     * Restarts whole game
     */
    public static void restartGame() {
        Audio.CHARGE.play();
        if(!displayPanel.isGoingToNextLevel()) {
            LEVEL = 1;
            PACMAN_LIVES = 2;
            SCORE = 0;
        } else {
            LEVEL++;
        }
        jFrame.remove(displayPanel);
        displayPanel = new DisplayPanel();
        jFrame.add(displayPanel);
        jFrame.pack();
        displayPanel.requestFocus();
    }
}