package gsm.shino.games.pacman;

import java.awt.Color;
import java.awt.Graphics;

/**
 * GameObject class is an abstract class.
 * Contains common attributes which all the game object including actors and map.
 */
public abstract class GameObject {

    private Color color;
    private int x;
    private int y;
     
    public GameObject(int x, int y, Color color){
        this.color = color;
        this.x = x;
        this.y = y;
    }
    
    protected Color getColor() {
        return color;
    }

    protected void setColor(Color color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public abstract void draw(Graphics graphics);
}
