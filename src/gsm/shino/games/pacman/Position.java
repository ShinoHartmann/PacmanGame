package gsm.shino.games.pacman;

/**
 * Define top, right, bottom and left position.
 * It is used when the Actor changes the direction to move to calculate the next x and y position.
 */
public enum Position {
    
    TOP(0), RIGHT(1), BOTTOM(2), LEFT(3);
    
    private final int index;
    
    Position(int index) {
        this.index = index;
    }
    
    public int getIndex() {
        return index;
    }

}
