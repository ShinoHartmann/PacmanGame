package gsm.shino.games.pacman.actor;

import gsm.shino.games.pacman.DisplayPanel;
import java.awt.Color;
import javax.swing.Timer;
import gsm.shino.games.pacman.GameObject;
import gsm.shino.games.pacman.map.Cell;
import gsm.shino.games.pacman.Position;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static gsm.shino.games.pacman.Position.*;
import java.awt.BasicStroke;
import java.awt.Graphics2D;

/**
 * Actor class is a parent class of Pacman and Ghost classes.
 * contains all the common behaviours of ghosts and Pacman.
 */
public abstract class Actor extends GameObject implements ActionListener {
    
    public static final int SPACE = 2;
    public static int DIAMETER = Cell.CELL_WIDTH - SPACE*4;
    private final Timer movingTimer;
    private int xOffset;
    private int yOffset;
    private int stepDistance = 1;
    private Cell currentCell;
    private Cell previousCell;
    private Cell base;
    private Position direction;
    private Position nextDirection;
    
    public Actor(Cell currentCell, Color color, int speed, int startDelay, Position direction) {
        super(currentCell.getX(), currentCell.getY(), color);
        this.direction = direction;
        nextDirection = direction;
        this.currentCell = currentCell;
        base = currentCell;
        previousCell = currentCell;
        movingTimer = new Timer(speed, this);
        movingTimer.setInitialDelay(startDelay);
    }

    /**
     * This method is used in DisplayPanel class to start the moving timer for the actors.
     */
    public void startMoving() {
        movingTimer.start();
    }

    @Override
    public void draw(Graphics graphics) {
        ((Graphics2D) graphics).setStroke((new BasicStroke(1)));
        drawActor(graphics);
    }

    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(canChangeDirection()){
            direction = nextDirection;
            changeFaceDirection();
        }
        if(!isBlocked()) {
            move();
        }
    }

    /**
     * @return true if the actor can change the direction to the next direction.
     *         and return false if the actor can not change the direction to the next direction.
     */
    private boolean canChangeDirection() {
        boolean goingBack = isGoingBack(TOP, BOTTOM) || isGoingBack(LEFT, RIGHT);
        boolean noUpcomingWall = isInCellPosition(currentCell) && !currentCell.hasWall(nextDirection);
        return goingBack || noUpcomingWall;
    }

    /**
     * @param fromDirection direction of where the actor is going at the moment
     * @param toDirection direction where the actor will go next
     * @return true if toDirection and from direction is opposite direction.
     */
    private boolean isGoingBack(Position fromDirection, Position toDirection) {
        if(direction == fromDirection && nextDirection == toDirection) {
            return true;
        } else if(direction == toDirection && nextDirection == fromDirection) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param cell to check the position of x and y.
     * @return whether the actor is at the exactly same x and y position as cell.
     */
    private boolean isInCellPosition(Cell cell) {
        return getX() == cell.getX() && getY() == cell.getY();
    }

    /**
     * Changes next direction of the actor.
     * @param pos direction where the actor will go next.
     */
    protected void changeNextDirection(Position pos) {
        nextDirection = pos;
    }

    /**
     * @return true if the actor is not at the exactly same x and y as current cell
     *         or if there is a wall on the direction.
     * */
    private boolean isBlocked() {
        return isInCellPosition(currentCell) && currentCell.hasWall(direction);
    }

    /**
     * Changes the x and y position of the actor depending on the direction where the actor is going.
     */
    private void move() {
        switch(direction) {
            case TOP: 
                yOffset = -stepDistance;
                xOffset = 0;
                break;
            case BOTTOM:
                yOffset = stepDistance;
                xOffset = 0;
                break;
            case LEFT:
                yOffset = 0;
                xOffset = -stepDistance;
                break;
            case RIGHT:
                yOffset = 0;
                xOffset = stepDistance;
        }
        setX(getX() + xOffset);
        setY(getY() + yOffset);
        beamOffMap();
        moveToNeighborCell();
        positionMoved();
    }

    /**
     * Changes the current cell position.
     * The actor changes the current cell when he is at the exactly same x and y as neighbor cell
     */
    private void moveToNeighborCell() {
        Cell neighborCell = currentCell.getNeighbor(direction);
        if(isInCellPosition(neighborCell)) {
            previousCell = currentCell;
            currentCell = neighborCell;
            cellChanged();
        }
    }

    /**
     * As the map is looping, the actor is able to exit the panel from the right side and enter to the left side.
     */
    private void beamOffMap() {
        int border = Cell.CELL_WIDTH/2;
        if(getX() > (DisplayPanel.GAME_WIDTH - border)) {
            setX(-border);
        } else if(getX() < - border) {
            setX(DisplayPanel.GAME_WIDTH - border);
        }
    }

    /**
     * When a ghost hit Pacman, all the actors go back to their base and start moving from there again.
     */
    protected void goBackToBase() {
        setX(base.getX());
        setY(base.getY());
        currentCell = base;
        firstDirection();
    }

    /**
     * @param point1 point to get the distance from
     * @param point2 point to get the distance from
     * @return integer of the distance between point1 and point2
     */
    protected int distance(int point1, int point2) {
        return Math.abs(point1 - point2);
    }

    protected abstract void drawActor(Graphics graphics);
    protected abstract void changeFaceDirection();
    protected abstract void positionMoved();
    protected abstract void cellChanged();
    protected abstract void firstDirection();

    public void setStepDistance(int stepDistance) {
        this.stepDistance = stepDistance;
    }
    public Cell getCurrentCell() {
        return currentCell;
    }
    public Cell getPreviousCell() {
        return previousCell;
    }
    public Position getDirection() {
        return direction;
    }
    public void setDirection(Position direction) {
        this.direction = direction;
    }
}
