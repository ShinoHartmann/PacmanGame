package gsm.shino.games.pacman.actor;

import gsm.shino.games.pacman.DisplayPanel;
import static gsm.shino.games.pacman.Position.*;

import gsm.shino.games.pacman.Game;
import gsm.shino.games.pacman.audio.Audio;
import gsm.shino.games.pacman.map.Cell;
import gsm.shino.games.pacman.map.Map;
import gsm.shino.games.pacman.Position;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Random;
import javax.swing.Timer;

/**
 * Ghost class contains all ghosts behaviours.
 */
public class Ghost extends Actor {
        
    private final Timer legsTimer;
    private final DisplayPanel displayPanel;
    private final Pacman pacman;
    private final GhostPainter ghostPainter; 
    private final Random random = new Random();
    private boolean vulnerable = false;
    private Map map;
    private List<Ghost> ghosts;
    private Position initialDirection = Position.RIGHT;
    
    public Ghost(DisplayPanel displayPanel, Pacman pacman, Map map, Color color, int speed) {
        super(map.getEnemyBase(), color, speed, new Random().nextInt(100)*100, Position.RIGHT);
        ghostPainter = new GhostPainter(getX()+SPACE, getY()+SPACE*2, getColor(), getDirection());
        legsTimer = new Timer(200, new Ghost.LegsMover());
        legsTimer.start();
        this.pacman = pacman;
        this.displayPanel = displayPanel;
        ghosts = displayPanel.getGhosts();
    }

    /**
     * Sets the first direction of a ghost.
     * To avoid a bug, the direction of the first direction of ghosts is always right
     */
    protected void firstDirection() {
        setDirection(Position.RIGHT);
    }

    /**
     * Checks Pacman's position after moving.
     */
    @Override
    protected void positionMoved() {
        checkPacmanPosition();
    }

    /**
     * Checks whether the ghost hit Pacman or not and whether the ghost can see Pacman or not.
     * 1. If the ghost hits Pacman who has more than 1 life,
     * all ghosts and Pacman go back to their base and Pacman lost one life.
     * 2. If the ghost hits Pacman who does not have any lives left,
     * game is over.
     * 3. If the ghost can see Pacman, he will start following Pacman
     */
    private void checkPacmanPosition() {
        if(hasHitPacman()) {
            if(Game.PACMAN_LIVES> 0) {
                for(Ghost ghost: ghosts) {
                    ghost.goBackToBase();
                }
                pacman.goBackToBase();
                Game.PACMAN_LIVES--;
                Audio.HIT.play();
            } else {
                displayPanel.finishGame(false, this);
            }
        } else if(canSeePacman()) {
            chasePacman();
        }
    }

    /**
     * @return true if the ghost is hit by Pacman
     */
    private boolean hasHitPacman() {
        return !displayPanel.isGameFinished() && isPacmanInSameCell();
    }

    /**
     * @return true if Pacman's current cell is the same as the ghost's current cell.
     */
    private boolean isPacmanInSameCell() {
        return distance(getX(), pacman.getX()) < 20
                && distance(getY(), pacman.getY()) < 20;
    }

    /**
     * @return true if Pacman is not more than two cells away and the ghost is going to the direction where Pacman is.
     */
    private boolean canSeePacman() {
        Cell neighborCell = getCurrentCell().getNeighbor(getDirection());
        Cell nextNeighbor = neighborCell.getNeighbor(getDirection());
        if(canSeePacmanStraight(getCurrentCell(), neighborCell)
                || canSeePacmanStraight(neighborCell, nextNeighbor)) {
            return true;
        }
        return false;
    }

    /**
     * @param currentCell current cell
     * @param nextCell neighbor cell of currentCell
     * @return true if there is no walls where the ghost is going and Pacman is in nextCell
     */
    private boolean canSeePacmanStraight(Cell currentCell, Cell nextCell) {
        return !currentCell.hasWall(getDirection()) && pacman.getCurrentCell() == nextCell;
    }

    /**
     * This is unfinished method. I did not have time to implement it.
     * It should be able to chase Pacman
     */
    private void chasePacman() {
        //No implementation yer
    }

    /**
     * Changes the place of ghost eyes place depending on the direction where the ghost is going.
     */
    @Override
    protected void changeFaceDirection() {
        switch(getDirection()) {
            case TOP:
                ghostPainter.setDirection(TOP);
                break;
            case BOTTOM:
                ghostPainter.setDirection(BOTTOM);
                break;
            case LEFT:
                ghostPainter.setDirection(LEFT);
                break;
            case RIGHT:
                ghostPainter.setDirection(RIGHT);
        }
    }

    /**
     * Draws the ghost. The object is from GhostPainter class.
     * @see GhostPainter#draw(Graphics).
     * @param graphics
     */
    @Override
    public void drawActor(Graphics graphics) {
        ghostPainter.setX(getX()+SPACE);
        ghostPainter.setY(getY()+SPACE*2);
        ghostPainter.draw(graphics);
    }

    /**
     * Changes direction when a ghost is able to change direction.
     * Going backward will be avoided except the current cell is dead end.
     */
    @Override
    protected void cellChanged() {
        Position newDirection = null;
        do {
            newDirection = Position.values()[random.nextInt(4)];
        } while (isBlocked(newDirection));
        changeNextDirection(newDirection);
    }

    /**
     * @param newDirection next possible direction
     * @return true if the ghost is not able to go to the newDirection
     */
    private boolean isBlocked(Position newDirection) {
        if(!isDeadEnd()) {
            return getPreviousCell() == getCurrentCell().getNeighbor(newDirection)
                    || getCurrentCell().hasWall(newDirection);
        } else {
            return getCurrentCell().hasWall(newDirection);
        }
    }

    /**
     * @return true if the ghost has no other ways available except going backward.
     */
    private boolean isDeadEnd() {
        for(Position direction : Position.values()) {
            if(!(getPreviousCell() == getCurrentCell().getNeighbor(direction))) {
                if(!getCurrentCell().hasWall(direction)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Inner class to animate the ghost legs.
     * @see GhostPainter#setFirstMove(boolean)
     */
    private class LegsMover implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(ghostPainter.isFirstMove()) {
                ghostPainter.setFirstMove(false);
            } else {
                ghostPainter.setFirstMove(true);
            }
        }
    }
}
