package gsm.shino.games.pacman.actor;

import static gsm.shino.games.pacman.actor.Actor.DIAMETER;
import gsm.shino.games.pacman.Position;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

/**
 * contains code for painting ghosts.
 * This is a separate unit of code and is separated from Ghost behaviour code.
 */
public class GhostPainter{
    
    private final int width = DIAMETER;
    private final int faceHeight = (int) (width * 0.85);
    private final int height = (int) (faceHeight*1.2);
    private final int footWidth = (int) (width*0.08);
    private final int eyeSize = width/4;
    private final int eyeBallSize = eyeSize*3/4;
    private final Color color;
    private int x;
    private int y;
    private boolean firstMove = true;
    private Position direction;
    private Random random = new Random();
    private boolean female = random.nextBoolean();
    
    public GhostPainter(int x, int y, Color color, Position direction) {
        this.x = x;
        this.y = y;
        this.color = color;
        this.direction = direction;
    }

    /**
     * Draws a ghost.
     * Animates a ghost, there are first move and second move.
     * @param graphics
     */
    public void draw(Graphics graphics) {
        if(firstMove) {
            drawFirstMove(graphics);
        } else {
            drawSecondMove(graphics);
        }
        drawDefaultPolygon(graphics);        
        treatEyes(direction, graphics);
    }

    /**
     * Draws first movement of the legs.
     * @param graphics
     */
    private void drawFirstMove(Graphics graphics) {
        graphics.setColor(color);
        int[] xPoints = getFirstPolygonXPoints(x);
        int[] yPoints = getMovementYPoints(y);
        int nPoints = xPoints.length;
        graphics.drawPolygon(xPoints, yPoints, nPoints);
        graphics.fillPolygon(xPoints, yPoints, nPoints);
    }

    /**
     * @param startX first startX position
     * @return array list of x positions for first leg movement
     */
    private int[] getFirstPolygonXPoints(int startX) {
        return new int[] {
                startX,
                startX + width/4,
                startX + width/4 + footWidth,
                startX + width/2,
                startX + width/2,
                startX + width*3/4 - footWidth,
                startX + width*3/4,
                startX + width
        };
    }

    /**
     * @param startY first and second startY position
     * @return array list of y positions for first leg movement.
     */
    private int[] getMovementYPoints(int startY) {
        return new int[] {
                startY + faceHeight*3/4,
                startY + height,
                startY + height,
                startY + faceHeight - faceHeight/4,
                startY + faceHeight - faceHeight/4,
                startY + height,
                startY + height,
                startY + faceHeight*3/4
        };
    }

    /**
     * Draws second movement of the legs.
     * @param graphics
     */
    private void drawSecondMove(Graphics graphics) {
        graphics.setColor(color);
        int[] xPoints = getSecondPolygonXPoints(x);
        int[] yPoints = getMovementYPoints(y);
        int nPoints = xPoints.length;
        graphics.drawPolygon(xPoints, yPoints, nPoints);
        graphics.fillPolygon(xPoints, yPoints, nPoints);
    }

    /**
     * @param startX second startX position
     * @return array list of x positions for second leg movement
     */
    private int[] getSecondPolygonXPoints(int startX) {
        return new int[] {
                startX,
                startX + width*3/10,
                startX + width*3/10 + footWidth,
                startX + width/2 - footWidth/2,
                startX + width/2 + footWidth/2,
                startX + width*7/10 - footWidth,
                startX + width*7/10,
                startX + width
        };
    }

    /**
     * Draws default shape which does not change by first and second movement.
     * If the ghost is female, draw ribbon on the left corner.
     * @param graphics
     */
    private void drawDefaultPolygon(Graphics graphics) {
        graphics.setColor(color);
        graphics.drawArc(x, y, width, faceHeight, 0, 360);
        graphics.fillArc(x, y, width, faceHeight, 0, 360);
        graphics.drawRect(x, y + faceHeight/2, width, faceHeight/2);
        graphics.fillRect(x, y + faceHeight / 2, width, faceHeight * 23 / 40);
        
        int[] xPoints = getDefaultPolygonXPoints(x);
        int[] yPoints = getDefaultPolygonYPoints(y);
        int nPoint = getDefaultPolygonXPoints(x).length;
        graphics.setColor(color);
        graphics.drawPolygon(xPoints, yPoints, nPoint);
        graphics.fillPolygon(xPoints, yPoints, nPoint);
        if(female) {
            if(color == Color.RED) {
                changeGender(graphics, Color.YELLOW);
            } else {
                changeGender(graphics);
            }
        }
    }

    /**
     * @param startX x coordinaate of the polygon to create
     * @return array list of x positions which helps with drawing the default polygon
     */
    private int[] getDefaultPolygonXPoints(int startX) {
        return new int[] {
                startX,
                startX,
                startX + footWidth,
                startX + width/2,
                startX + width - footWidth,
                startX + width,
                startX + width
        };
    }

    /**
     * @param startY y coordinaate of the polygon to create
     * @return array list of x positions which helps with drawing the default polygon.
     */
    private int[] getDefaultPolygonYPoints(int startY) {
        return new int[] {
                startY + faceHeight/2,
                startY + height,
                startY + height,
                startY + faceHeight/2,
                startY + height,
                startY + height,
                startY + faceHeight/2,
        };
    }

    /**
     * Draws red ribbon.
     * @param graphics
     */
    private void changeGender(Graphics graphics) {
        changeGender(graphics, Color.RED);
    }

    /**
     * Draws ribbon on the left corner of the ghost.
     * @param graphics
     * @param color returns ribbon's color.
     */
    private void changeGender(Graphics graphics, Color color) {
        graphics.setColor(color);
        int[] xPoints = getRibbonXPoints(width / 4);
        int[] yPoints = getRibbonYPoints(height / 4);
        int nPoints = xPoints.length;
        graphics.drawPolygon(xPoints, yPoints, nPoints);
        graphics.fillPolygon(xPoints, yPoints, nPoints);
    }

    /**
     * @param ribbonWidth width of ribbon
     * @return array of x positions to draw ribbon
     */
    private int[] getRibbonXPoints(int ribbonWidth) {
        return new int[] {
                x,
                x + ribbonWidth,
                x + ribbonWidth,
                x
        };
    }

    /**
     * @param ribbonHeight height of ribbon
     * @return array of y positions to draw ribbon
     */
    private int[] getRibbonYPoints(int ribbonHeight) {
        return new int[] {
                y,
                y + ribbonHeight,
                y,
                y + ribbonHeight
        };
    }

    /**
     * Draws the ghost's eyes depending on the direction.
     * @param direction where the ghost is going to
     * @param graphics
     */
    private void treatEyes(Position direction, Graphics graphics) {
        switch(direction) {
            case TOP: 
                drawEyesUp(graphics);
                break;
            case BOTTOM:
                drawEyesDown(graphics);
                break;
            case LEFT:
                drawEyesLeft(graphics);
                break;
            case RIGHT:
                drawEyesRight(graphics);
        }
    }

    /**
     * Draws eyes of watching up
     * @param graphics
     */
    private void drawEyesUp(Graphics graphics){
        int leftEyeY = y + faceHeight/4 - eyeSize/4;
        int eyeBallY = leftEyeY - eyeBallSize / 4;
        drawVerticalEyes(graphics, leftEyeY, eyeBallY);
    }

    /**
     * Draws eyes of watching down
     * @param graphics
     */
    private void drawEyesDown(Graphics graphics) {
        int leftEyeY = y + faceHeight/4 + eyeSize/2;
        int eyeBallY = leftEyeY + eyeBallSize /4;
        drawVerticalEyes(graphics, leftEyeY, eyeBallY);
    }

    /**
     * Draws eyes when the ghost goes up or down
     * @param graphics
     * @param leftEyeY the start y position of the eyes on the left side
     * @param eyeBallY start y position of eye ball on the left side
     */
    private void drawVerticalEyes(Graphics graphics, int leftEyeY, int eyeBallY) {
        int leftEyeX = x + width/4 - eyeSize/4 ;
        int eyeHeight = eyeSize*3/4;
        drawEyes(graphics, leftEyeX, leftEyeY, eyeBallY, eyeHeight, 0);
    }

    /**
     * Draws eyes of watching left.
     * @param graphics
     */
    private void drawEyesLeft(Graphics graphics) {
        int leftEyeX = x + width*1/8;
        int left = -eyeBallSize / 2;
        drawHorizontalEyes(graphics, leftEyeX, left);
    }

    /**
     * Draws eyes of watching right.
     * @param graphics
     */
    private void drawEyesRight(Graphics graphics) {
        int leftEyeX = x + width*2/8;
        int right = eyeBallSize / 2;
        drawHorizontalEyes(graphics, leftEyeX, right);
    }

    /**
     * Draws eyes when the ghost goes right or left
     * @param graphics
     * @param horizontalPosition integer to adjust start x positions of eye ball
     */
    private void drawHorizontalEyes(Graphics graphics, int leftEyeX, int horizontalPosition) {
        int leftEyeY = y + faceHeight/3;
        int eyeBallY = leftEyeY + eyeSize/2 - eyeBallSize /4;
        int eyeHeigth = eyeSize*4/3;
        drawEyes(graphics, leftEyeX, leftEyeY, eyeBallY, eyeHeigth, horizontalPosition);
    }

    /**
     * Draws ghost eyes.
     * @param graphics
     * @param leftEyeX x position of the left eye.
     * @param leftEyeY y position of the left eye.
     * @param eyeHeight height of the eye.
     * @param horizontalPosition integer to adjust start x positions of eye ball.
     */
    private void drawEyes(Graphics graphics, int leftEyeX, int leftEyeY, int eyeBallY, int eyeHeight, int horizontalPosition) {
        int rightEyeX = leftEyeX + eyeSize + width/8;
        int eyeBallLeftX = leftEyeX + eyeSize/2 -  eyeBallSize /2 + horizontalPosition;
        int eyeBallRightX = rightEyeX + eyeSize/2 -  eyeBallSize /2 + horizontalPosition;
        drawWhiteEyes(graphics, leftEyeX, leftEyeY, eyeHeight, rightEyeX);
        drawEyeBalls(graphics, eyeBallY, eyeBallLeftX, eyeBallRightX);
    }

    /**
     * Draws white place of the ghost's eyes.
     * @param graphics
     * @param leftEyeX x position of the left eye.
     * @param leftEyeY y position of the left eye.
     * @param eyeHeight height of the eye.
     * @param rightEyeX x position of the right eye.
     */
    private void drawWhiteEyes(Graphics graphics, int leftEyeX, int leftEyeY, int eyeHeight, int rightEyeX) {
        graphics.setColor(Color.WHITE);
        graphics.drawArc(leftEyeX, leftEyeY, eyeSize, eyeHeight, 0, 360);
        graphics.fillArc(leftEyeX, leftEyeY, eyeSize, eyeHeight, 0, 360);
        graphics.drawArc(rightEyeX, leftEyeY, eyeSize, eyeHeight, 0, 360);
        graphics.fillArc(rightEyeX, leftEyeY, eyeSize, eyeHeight, 0, 360);
    }

    /**
     * Draws eyeball of the ghost's the eyes.
     * @param graphics
     * @param eyeBallY position of the eyeball position
     * @param eyeBallLeftX x position of the left eyeball.
     * @param eyeBallRightX y position of the left eyeball.
     */
    private void drawEyeBalls(Graphics graphics, int eyeBallY, int eyeBallLeftX, int eyeBallRightX) {
        graphics.setColor(Color.BLACK);
        graphics.drawArc(eyeBallLeftX, eyeBallY, eyeBallSize, eyeBallSize, 0, 360);
        graphics.fillArc(eyeBallLeftX, eyeBallY, eyeBallSize, eyeBallSize, 0, 360);
        graphics.drawArc(eyeBallRightX, eyeBallY, eyeBallSize, eyeBallSize, 0, 360);
        graphics.fillArc(eyeBallRightX, eyeBallY, eyeBallSize, eyeBallSize, 0, 360);
    }

    public boolean isFirstMove() {
        return firstMove;
    }
    public void setFirstMove(boolean firstMove) {
        this.firstMove = firstMove;
    }
    public void setDirection(Position direction) {
        this.direction = direction;
    }
    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }
}
