package gsm.shino.games.pacman.actor;

import static gsm.shino.games.pacman.Position.*;
import gsm.shino.games.pacman.DisplayPanel;
import gsm.shino.games.pacman.Game;
import gsm.shino.games.pacman.audio.Audio;
import gsm.shino.games.pacman.food.Food;
import gsm.shino.games.pacman.map.Cell;
import gsm.shino.games.pacman.map.Map;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.Timer;

/**
 * Contains Pacman's appearance and behaviours.
 * @Implements KeyListener to receive  keyboard input from the Player to control Pacman
 */
public class Pacman extends Actor implements KeyListener {
    
    private final Timer mouthTimer;
    private final Map map;
    private final Food[][] foods;
    private final DisplayPanel displayPanel;
    private int mouth = 300;
    private int faceDirection;
    private int eatableFood = 0;
    private int eatenFood = 0;
    
    public Pacman(DisplayPanel displayPanel, Map map, int speed) {
        super(map.getPacmanBase(), Color.YELLOW, speed, 0, BOTTOM);
        setColor(Color.YELLOW);
        this.map = map;
        this.foods = map.getFoods();
        mouthTimer = new Timer(300, new MouseMover());
        mouthTimer.start();
        firstDirection();
        this.displayPanel = displayPanel;
        displayPanel.addKeyListener(this);
        eatableFood = countEatableFood();
        eatFood(false);
    }

    /**
     * Sets first direction when the game starts.
     * Direction is set bottom to stop Pacman from moving without user input.
     */
    @Override
    protected void firstDirection() {
        faceDirection = FaceDirection.LEFT.getIndex();
        setDirection(BOTTOM);
    }

    /**
     * Checks whether Pacman is able to eat food or not every time after moving.
     */
    @Override
    protected void positionMoved() {
        eatFood(true);
    }

    /**
     * @return integer of how many food Pacman is available to eat on current map.
     */
    private int countEatableFood() {
        int count = 0;
        for(Food[] foodColumns : foods) {
            for(Food food : foodColumns) {
                if(!food.isEaten()) {
                    count++;
                }
            }
        }
        return count;
    }

    /**
     * Lets Pacman to eat food. When Pacman eats food, the score increases.
     * @param addScore false to stop counting food at the Pacmans base when the game starts
     */
    private void eatFood(boolean addScore) {
        for(Food[] foodColumns : foods) {
            if(distance(calculateXPosition(foodColumns), getX()) < Cell.CELL_WIDTH/3 ) {
                for(Food food : foodColumns) {
                    if(canEatFood(food)) {
                        food.pick();
                        eatenFood++;
                        if(addScore) {
                            Audio.YAM.play();
                            Game.SCORE+= food.getScore();
                        }
                        break;
                    }
                }
            } 
        }
        if(eatenFood >= eatableFood) {
            displayPanel.finishGame(true, this);
        }
    }

    /**
     * @param foods the array of food
     * @return x position of foods
     */
    private int calculateXPosition(Food[] foods) {
        return foods[0].getX() - Cell.CELL_WIDTH/2;
    }

    /**
     * @param food food to check
     * @return true if Pacman is able to eat food
     */
    private boolean canEatFood(Food food) {
        return !food.isEaten() && distance(calculateYPosition(food), getY()) < Cell.CELL_WIDTH/3;
    }

    /**
     * @param food food to calculate position from
     * @return  y position of food
     */
    private int calculateYPosition(Food food) {
        return food.getY() - Cell.CELL_WIDTH/2;
    }

    /**
     * Lets the player control Pacman.
     * This keyboard input does not reflect until Pacman is able to change the direction.
     * @see Actor#canChangeDirection()
     * @param e
     */
    @Override
    public void keyPressed(KeyEvent e) {
        switch(e.getKeyCode()) {
            case KeyEvent.VK_UP:
                changeNextDirection(TOP);
                break;
            case KeyEvent.VK_DOWN:
                changeNextDirection(BOTTOM);
                break;
            case KeyEvent.VK_LEFT:
                changeNextDirection(LEFT);
                break;
            case KeyEvent.VK_RIGHT:
                changeNextDirection(RIGHT);
                break;
        }
    }

    /**
     * Draws the Pacman.
     * SPACE is used to draw Pacman smaller than the cell size.
     * @param graphics
     */
    @Override
    public void drawActor(Graphics graphics) {
        graphics.setColor(getColor());
        graphics.drawArc(getX()+SPACE, getY()+SPACE*2, DIAMETER, DIAMETER, faceDirection, mouth);
        graphics.fillArc(getX()+SPACE, getY()+SPACE*2, DIAMETER, DIAMETER, faceDirection, mouth);
    }

    /**
     * Changes the direction of Pacman's face depending on the direction where the ghost is going.
     */
    @Override
    public void changeFaceDirection() {
        switch(getDirection()) {
            case TOP: 
                faceDirection = FaceDirection.UP.getIndex();
                break;
            case BOTTOM:
                faceDirection = FaceDirection.DOWN.getIndex();
                break;
            case LEFT:
                faceDirection = FaceDirection.LEFT.getIndex();
                break;
            case RIGHT:
                 faceDirection = FaceDirection.RIGHT.getIndex();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {}
    @Override
    public void keyTyped(KeyEvent e) {}
    @Override
    protected void cellChanged() {}

    /**
     * Inner class to animate Pacman's mouse with opening and closing.
     */
    private class MouseMover implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(mouth == 360) {
                mouth = 300;
            } else {
                mouth = 360;
            }
        }
    }

    /**
     * Inner enum class which contains face direction of Pacman.
     */
    private enum FaceDirection {
        LEFT(210), RIGHT(30), UP(120), DOWN(300);
        
        private final int index;
        
        FaceDirection(int index) {
            this.index = index;
        }

        public int getIndex() {
            return index;
        }
    }
}
