package gsm.shino.games.pacman.audio;

import javax.sound.sampled.*;
import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Audio class takes care of effect music.
 */
public class Audio {

    public final static Audio YAM = new Audio("yam.wav");
    public final static Audio GAME_OVER = new Audio("gameOver.wav");
    public final static Audio WON = new Audio("won.wav");
    public final static Audio CHARGE = new Audio("charge.wav");
    public final static Audio HIT = new Audio("hit.wav");

    private final static ExecutorService THREAD_POOL = Executors.newFixedThreadPool(5);
    private final static int BUFFER_SIZE = 128000;
    private File soundFile;

    public Audio(String file) {
        try {
            soundFile = new File(Audio.class.getResource(file).toURI());
        } catch(Exception ex) {
            throw new RuntimeException("Error occurred while loading audio file: " + file);
        }
    }

    /**
     * Takes care of multiple effect music at the same time.
     */
    public void play() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                playAudio();
            }
        });
        THREAD_POOL.submit(thread);
    }

    /**
     * Plays the effect music.
     */
    private void playAudio() {
        try {
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(soundFile);
            AudioFormat audioFormat = audioStream.getFormat();

            DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
            SourceDataLine sourceLine = (SourceDataLine) AudioSystem.getLine(info);
            sourceLine.open(audioFormat);
            sourceLine.start();

            byte[] dataBuffer = new byte[BUFFER_SIZE];
            int bytesRead;
            while ((bytesRead = audioStream.read(dataBuffer, 0, dataBuffer.length)) != -1) {
                sourceLine.write(dataBuffer, 0, bytesRead);
            }

            sourceLine.drain();
            sourceLine.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
