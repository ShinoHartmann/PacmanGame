
package gsm.shino.games.pacman.food;

import java.awt.Color;
import java.awt.Graphics;
import gsm.shino.games.pacman.GameObject;

/**
 * Food class is food which Pacman eats on the map.
 * Contains its value points.
 */
public class Food extends GameObject {

    private int score = 5;
    private int size = 3;
    private boolean eaten = false;
    private static final Color color = new Color(255, 170, 164);
    
    public Food(int x, int y) {
        super(x, y, color);
        setColor(color);
        setX(x);
        setY(y);
    }
    
    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(getColor());
        graphics.drawOval(getX() - size/2 , getY() - size/2 , size, size);
        graphics.fillOval(getX() - size/2 , getY() - size/2 , size, size);
    }

    public int getScore() {
        return score;
    }
    public void setScore(int score) {
        this.score = score;
    }
    public void setSize(int size) {
        this.size = size;
    }
    public boolean isEaten() {
        return eaten;
    }
    public void pick() {
       eaten = true;
    }    
}
