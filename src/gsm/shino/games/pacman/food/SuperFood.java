package gsm.shino.games.pacman.food;

import gsm.shino.games.pacman.map.Cell;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

/**
 * SuperFood class has different points, colour and size of the food.
 */
public class SuperFood extends Food{
    
    private static final int SCORE = 20;
    private static final int SIZE = 8;
    private final Timer blinkTimer;
    boolean isEaten = false;
    private final Color color = new Color(255, 170, 164);
    
    public SuperFood(int x, int y) {
        super(x, y);
        setSize(SIZE);
        setScore(SCORE);
        blinkTimer = new Timer(300, new Blinking());
        blinkTimer.start();
    }
    
    @Override
    public void draw(Graphics graphics) {
        graphics.setColor(getColor());
        graphics.drawOval(getX() - SIZE/2, getY() - SIZE/2, SIZE, SIZE);
        graphics.fillOval(getX() - SIZE/2, getY() - SIZE/2, SIZE, SIZE);
    }

    /**
     * Inner class to blink the superfood.
     * This class is used when blinkTimer is out.
     */
    private class Blinking implements ActionListener {
        
        @Override
        public void actionPerformed(ActionEvent e) {
            if(getColor() == color) {
            setColor(Cell.CELL_COLOR);
            } else {
                setColor(color);
            }
        }
    }
}
