package gsm.shino.games.pacman.map;

import java.awt.*;

import gsm.shino.games.pacman.GameObject;
import gsm.shino.games.pacman.Position;

import static gsm.shino.games.pacman.Position.*;

/**
 * Cell class contains reference of top, right, bottom and left neighbour�s cells
 * and Information of whether it has a wall on its sides
 */
public class Cell extends GameObject {

    public static final int CELL_WIDTH = 40;
    public static final int WALL_WIDTH = 8;
    public static final int TOTAL_WIDTH = CELL_WIDTH + WALL_WIDTH;
    public static final Color CELL_COLOR = Color.BLACK;
    private final boolean[] cellWalls = new boolean[4];
    private final Map map;
    private final Cell[][] cells;
    private final int columnIndex;
    private final int rowIndex;
    private Cell[] neighbors;

    public Cell(Map map, int x, int y) {
        super(x, y, CELL_COLOR);
        this.map = map;
        this.cells = map.getCells();
        setX(x);
        setY(y);
        setColor(CELL_COLOR);
        columnIndex = (getX() - WALL_WIDTH) / TOTAL_WIDTH;
        rowIndex = (getY() - WALL_WIDTH - Map.STATUS_PANEL_HEIGHT) / TOTAL_WIDTH;
    }

    /**
     * @param pos specify which position of the neighbor to get.
     * @return neighbor cell of the cell.
     *      It also set all the neighbor cells of the cell.
     */
    public Cell getNeighbor(Position pos) {
        if(neighbors == null) {
            assignNeighbors();
        }
        return neighbors[pos.getIndex()];
    }

    /**
     * Assigns all the neighbor cells of the cell.
     */
    private void assignNeighbors() {
        neighbors = new Cell[4];
        for(Position position : Position.values()) {
            neighbors[position.getIndex()] = assignNeighbor(position);
        }
    }
    /**
     * @param pos specify which position of the neighbor to get.
     * @return the neighbor cell of the pos
     */
    private Cell assignNeighbor(Position pos) {
        int neighborColumnIndex = columnIndex;
        int neighborRowIndex = rowIndex;
        switch(pos) {
            case TOP:
                neighborRowIndex -= 1;
                break;
            case BOTTOM:
                neighborRowIndex += 1;
                break;
            case LEFT:
                neighborColumnIndex -= 1;
                break;
            case RIGHT:
                neighborColumnIndex += 1;
                break;
        }
        neighborColumnIndex = getNeighborIndex(neighborColumnIndex, Map.COLUMNS);
        neighborRowIndex = getNeighborIndex(neighborRowIndex, map.getRows());
        return cells[neighborColumnIndex][neighborRowIndex];
    }

    /**
     * Gets neighbor's index to assign neighbor of the cell.
     * @param index index number of the cell
     * @param maxIndex maximum possible number of the cells on the map
     * @return neighbor's index
     */
    private int getNeighborIndex(int index, int maxIndex) {
        if(index == -1) {
            return maxIndex - 1;
        } else if(index == maxIndex) {
            return 0;
        } else {
            return index;
        }
    }

    @Override
    public void draw(Graphics graphics) {
        paintCell(graphics);
        paintWall(graphics);
    }

    /**
     * Paints the cell.
     * @param graphics
     */
    private void paintCell(Graphics graphics) {
        graphics.setColor(Color.black);
        graphics.fillRect(getX(), getY(), CELL_WIDTH, CELL_WIDTH);
    }

    /**
     * 1. Paints walls of the cell if there is a wall.
     * 2. Paints extra wall between the cell and the neighbor cell if applicable.
     * @param graphics
     */
    private void paintWall(Graphics graphics) {
        int x = getX();
        int y = getY();
        graphics.setColor(new Color(0, 235, 255));
        for(int positionIndex = TOP.getIndex(); positionIndex <= LEFT.getIndex(); positionIndex++) {
            int[] paintPosition;
            if(cellWalls[positionIndex]) {
                if(positionIndex == TOP.getIndex()) {
                    // paint horizontal line from top left corner to top right corner.
                    paintPosition = assignValuesToPaintWall(x, y, x + CELL_WIDTH, y);
                } else if(positionIndex == RIGHT.getIndex()) {
                    // paint vertical line from top right corner to bottom right corner.
                    paintPosition = assignValuesToPaintWall(x + CELL_WIDTH, y, x + CELL_WIDTH, y + CELL_WIDTH);
                } else if(positionIndex == BOTTOM.getIndex()) {
                    // paint horizontal line from bottom left corner to bottom right corner.
                    paintPosition = assignValuesToPaintWall(x, y + CELL_WIDTH, x + CELL_WIDTH, y + CELL_WIDTH);
                } else {
                    // paint vertical line from top left corner to bottom left corner.
                    paintPosition = assignValuesToPaintWall(x, y, x, y + CELL_WIDTH);
                }
                graphics.drawLine(paintPosition[0], paintPosition[1], paintPosition[2], paintPosition[3]);
            } else {
                if(positionIndex == TOP.getIndex()) {
                    // paint vertical line from top left corner to the top neighbor.
                    paintPosition = assignValuesToPaintWall(x, y - WALL_WIDTH, x, y);
                } else if(positionIndex == RIGHT.getIndex()) {
                    // paint horizontal line from top right corner to the right neighbor.
                    paintPosition = assignValuesToPaintWall(x + CELL_WIDTH, y, x + TOTAL_WIDTH, y);
                } else if(positionIndex == BOTTOM.getIndex()) {
                    // paint vertical line from bottom right corner to the bottom neighbor.
                    paintPosition = assignValuesToPaintWall(x + CELL_WIDTH, y + CELL_WIDTH, x + CELL_WIDTH, y + TOTAL_WIDTH);
                } else {
                    // paint horizontal line from bottom left corner to the left neighbor.
                    paintPosition = assignValuesToPaintWall(x, y + CELL_WIDTH, x - WALL_WIDTH, y + CELL_WIDTH);
                }
                graphics.drawLine(paintPosition[0], paintPosition[1], paintPosition[2], paintPosition[3]);
            }
        }
    }

    /**
     * Assigns values to paint walls.
     * @param startX first x point of the line.
     * @param startY  first y point of the line.
     * @param endX end x point of the line.
     * @param endY end y point of the line.
     * @return an integer array contains all the necessary points to draw a line.
     */
    private int[] assignValuesToPaintWall(int startX, int startY, int endX, int endY) {
        return new int[]{
                startX,
                startY,
                endX,
                endY
        };
    }

    /**
     * Draws either right or left edge of the wall.
     * @param graphics
     * @param column column number of the cell.
     */
    public void paintExceptionWall(Graphics graphics, int column) {
        int[] paintPosition;
        int x = getX();
        int y = getY();
        if(column == 0) {
            paintPosition = assignValuesToPaintWall(x - WALL_WIDTH, y, x, y);
        } else{
            paintPosition = assignValuesToPaintWall(x + CELL_WIDTH, y + CELL_WIDTH, x + TOTAL_WIDTH, y + CELL_WIDTH);
        }
        graphics.drawLine(paintPosition[0], paintPosition[1], paintPosition[2], paintPosition[3]);
    }

    public boolean hasWall(Position pos) {
        return cellWalls[pos.getIndex()];
    }

    public void addWall(Position pos) {
        cellWalls[pos.getIndex()] = true;
    }

    public void removeWall(Position pos) {
        cellWalls[pos.getIndex()] = false;
    }


}
