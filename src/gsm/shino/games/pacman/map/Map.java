package gsm.shino.games.pacman.map;
import gsm.shino.games.pacman.DisplayPanel;
import gsm.shino.games.pacman.Position;
import gsm.shino.games.pacman.food.Food;
import gsm.shino.games.pacman.food.SuperFood;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import static gsm.shino.games.pacman.Position.*;
import java.awt.Color;

/**
 * Map class is an abstract class which creates all the cells  and places food
 */
public abstract class Map {
    
    public static final int STATUS_PANEL_HEIGHT = 50;
    public static int COLUMNS = 13;
    public int rows;
    private Cell[][] cells;
    private Food[][] foods;
    private Image offlineImage;
    private Cell enemyBase;
    private Cell pacmanBase;
    
    public Map(int rows) {
        this.rows = rows;
        cells = new Cell[COLUMNS][rows];
        drawMapToOfflineImage();
        placeAllFood();
    }
    
    public void draw(Graphics graphics) {
        graphics.setColor(Color.WHITE);
        graphics.drawImage(offlineImage, 0, 0, new Component() {
        });
        drawFood(graphics);
    }

    /**
     * 1. Creates and paints cells and walls to create the map
     * 2. Makes the map offline.
     */
    private void drawMapToOfflineImage() {
        offlineImage = new BufferedImage(DisplayPanel.DISPLAY_WIDTH, DisplayPanel.DISPLAY_HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics graphics = offlineImage.getGraphics();
        buildCells();
        drawCells(graphics);
        paintExceptionCellWalls(graphics, cells);
        paintEnemyBase(graphics);
    }

    /**
     * 1. assign all the cells.
     * 2. builds cell's walls.
     * 3. Sets all the actor's base.
     */
    private void buildCells() {
        cells = new Cell[COLUMNS][rows];
        for(int column = 0; column < cells.length; column++) {
            for(int row = 0; row < cells[column].length; row++) {
                int x = column * Cell.CELL_WIDTH + (column + 1) * Cell.WALL_WIDTH;
                int y = (row * Cell.CELL_WIDTH + (row + 1) * Cell.WALL_WIDTH) + STATUS_PANEL_HEIGHT;
                cells[column][row] = new Cell(this, x, y);
            }
        }
        buildCellWalls(cells);
        setActorsBase(cells);
    }

    /**
     * paints all cells on the map.
     * @param graphics
     */
    private void drawCells(Graphics graphics) {
        for(Cell[] column: cells) {
            for(Cell cell: column) {
                cell.draw(graphics);
            }
        }
    }

    /**
     * Paints the enemy's base
     * @param graphics
     */
    protected void paintEnemyBase(Graphics graphics) {
        graphics.setColor(Color.GRAY);
        graphics.fillRect(enemyBase.getX(), enemyBase.getY() - Cell.WALL_WIDTH, Cell.CELL_WIDTH, Cell.WALL_WIDTH);
    }

    /**
     *  Adds cell's wall
     * @param cells dimension array of dells
     * @param pos position which is added wall
     * @param column column index of the cell
     * @param rows integer array of row indexes of the cell
     */
    protected void addCellWall(Cell[][] cells, Position pos, int column, int... rows) {
        for(int row : rows) {
            cells[column][row].addWall(pos);
        }
    }
    /**
     * Sets walls which are around the map.
     * Sets left and top neighbour walls.
     * @param cells
     */
    protected void buildAutoWalls(Cell[][] cells) {
        for (int column = 0; column < cells.length; column++) {
            for (int row = 0; row < cells[column].length; row++) {
                if (column == 0) {
                    cells[column][row].addWall(LEFT);
                }
                if (column == COLUMNS -1) {
                    cells[column][row].addWall(RIGHT);
                }
                if (row == 0) {
                    cells[column][row].addWall(TOP);
                }
                if (row == rows-1) {
                    cells[column][row].addWall(BOTTOM);
                }
                if (cells[column][row].getNeighbor(TOP).hasWall(BOTTOM)) {
                    cells[column][row].addWall(TOP);
                }
                if (cells[column][row].getNeighbor(LEFT).hasWall(RIGHT)) {
                    cells[column][row].addWall(LEFT);
                }
            }
        }
    }

    /**
     * Draws food if it is not eaten
     * @param graphic
     */
    public void drawFood(Graphics graphic) {
        for (Food[] column : foods) {
             for (Food food : column) {
                 if (!food.isEaten()) {
                     food.draw(graphic);
                 }
             }
        }
    }

    /**
     * 1. Places all food on the map.
     * 2. remove all food which is specified on the map.
     * 3. Places all superfood which is specified on the map.
     */
    private void placeAllFood() {
        foods = new Food[COLUMNS][rows];
        for(int column = 0; column < foods.length; column++) {
            for(int row = 0; row < foods[column].length; row++) {
                placeFood(column, row);
            }
        }
        removeFoods();
        placeSuperFoods();
    }

    /**
     * Places food.
     * @param column column index of the cell
     * @param row row index of the cell
     */
    private void placeFood(int column, int row) {
        int x =  cells[column][row].getX() + Cell.CELL_WIDTH/2;
        int y = cells[column][row].getY() + Cell.CELL_WIDTH/2;
        foods[column][row] = new Food(x, y);
    }

    /**
     * Places super food.
     * @param column ccolumn index of the cell
     * @param row row index of the cell
     */
    protected void placeSuperFood(int column, int row) {
        int x =  cells[column][row].getX() + Cell.CELL_WIDTH/2;
        int y = cells[column][row].getY() + Cell.CELL_WIDTH/2;
        foods[column][row] = new SuperFood(x, y);
    }

    /**
     * Removes food on the specified column with multiple rows.
     * @param column column index of the cells
     * @param rows array of the row indexes of the cells
     */
    protected void removeCellFood(int column, int... rows) {
        for(int row : rows) {
            removeFood(column, row);
        }
    }

    /**
     * Removes food on the specified column and row.
     * @param column
     * @param row
     */
    protected void removeFood(int column, int row) {
        foods[column][row].pick();
    }

    /**
     * Paints both right and left edge of the wall.
     * @param graphics
     * @param cells returns a dimension array of the cells on the map
     */
    protected abstract void paintExceptionCellWalls(Graphics graphics, Cell[][] cells);

    /**
     * Define walls for each cell.
     * @param cells
     */
    protected abstract void buildCellWalls(Cell[][] cells);

    /**
     * Sets all the actors' base.
     * @param cells
     */
    protected abstract void setActorsBase(Cell[][] cells);

    /**
     * Specifies where to place all the superfood
     */
    protected abstract void placeSuperFoods();

    /**
     * Specifies where to remove all the food are
     */
    protected abstract void removeFoods();

    public Cell[][] getCells() {
        return cells;
    }
    public Food[][] getFoods() {
        return foods;
    }
    public int getRows() {
        return rows;
    }
    public Cell getEnemyBase() {
        return enemyBase;
    }
    public void setEnemyBase(Cell enemyBase) {
        this.enemyBase = enemyBase;
    }
    public Cell getPacmanBase() {
        return pacmanBase;
    }
    public void setPacmanBase(Cell pacmanBase) {
        this.pacmanBase = pacmanBase;
    }
}
