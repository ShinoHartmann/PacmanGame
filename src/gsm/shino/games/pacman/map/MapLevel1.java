package gsm.shino.games.pacman.map;

import static gsm.shino.games.pacman.Position.*;

import java.awt.Graphics;

/**
 * Contains all the details of stage 1
 */
public class MapLevel1 extends Map {
    
    public MapLevel1() {
        super(7);
    }

    /**
     * @see Map#buildCellWalls(Cell[][])
     * @param cells
     */
    @Override
    public void buildCellWalls(Cell[][] cells) {
        addCellWall(cells, BOTTOM, 0, 1, 2,3,4);
        addCellWall(cells, BOTTOM, 1, 0, 1, 2, 3, 4, 5);
        addCellWall(cells, RIGHT, 1, 2, 4);
        addCellWall(cells, RIGHT, 2, 0, 2, 3, 4);
        addCellWall(cells, BOTTOM, 3, 2, 5);
        addCellWall(cells, RIGHT, 3, 4);
        addCellWall(cells, BOTTOM, 4, 0, 1, 5);
        addCellWall(cells, RIGHT, 4, 2, 4);
        addCellWall(cells, BOTTOM, 5, 0, 1, 2, 4, 5);
        addCellWall(cells, BOTTOM, 6, 0, 1, 2,3, 4, 5);
        addCellWall(cells, BOTTOM, 7, 0, 1, 2, 4, 5);
        addCellWall(cells, RIGHT, 7, 2, 4);
        addCellWall(cells, BOTTOM, 8, 0, 1, 5);
        addCellWall(cells, RIGHT, 8, 4);
        addCellWall(cells, BOTTOM, 9, 2, 5);
        addCellWall(cells, RIGHT, 9, 0, 2, 3, 4);
        addCellWall(cells, RIGHT, 10, 2, 4);
        addCellWall(cells, BOTTOM, 11, 0, 1, 2, 3, 4, 5);
        addCellWall(cells, BOTTOM, 12, 1, 2, 3, 4);
        buildAutoWalls(cells);
        cells[0][3].removeWall(LEFT);
        cells[12][3].removeWall(RIGHT);
    }

    /**
     * @see Map#paintExceptionCellWalls(Graphics, Cell[][])
     * @param graphics
     * @param cells dimension array of the cells on the map
     */
    @Override
    protected void paintExceptionCellWalls(Graphics graphics, Cell[][] cells) {
        cells[0][3].paintExceptionWall(graphics, 0);
        cells[COLUMNS -1][3].paintExceptionWall(graphics, COLUMNS - 1);
    }

    /**
     * @see Map#setActorsBase(Cell[][])
     * @param cells
     */
    @Override
    protected void setActorsBase(Cell[][] cells) {
        setEnemyBase(cells[6][2]);
        cells[6][2].removeWall(TOP);
        setPacmanBase(cells[6][6]);
    }

    /**
     * @see Map#removeFoods()
     */
    @Override
    protected void removeFoods() {
        removeCellFood(0, 2,4);
        removeCellFood(1, 2, 4);
        removeCellFood(5, 2);
        removeCellFood(6, 2);
        removeCellFood(7, 2);
        removeCellFood(11, 2, 4);
        removeCellFood(12, 2, 4);
    }

    /**
     * @see Map#placeSuperFoods()
     */
    @Override
    protected void placeSuperFoods() {
        placeSuperFood(0, 0);
        placeSuperFood(0, 6);
        placeSuperFood(12, 0);
        placeSuperFood(12, 6);
    }
}
