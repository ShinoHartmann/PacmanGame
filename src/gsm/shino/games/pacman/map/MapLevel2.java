package gsm.shino.games.pacman.map;

import static gsm.shino.games.pacman.Position.*;

import java.awt.Graphics;

/**
 * Contains all the details of stage 2
 */
public class MapLevel2 extends Map{
    public MapLevel2() {
        super(9);
    }

    /**
     * @see Map#buildCellWalls(Cell[][])
     * @param cells
     */
    @Override
    protected void buildCellWalls(Cell[][] cells) {
        addCellWall(cells, BOTTOM, 0, 3, 4, 5, 6);
        addCellWall(cells, RIGHT, 0, 1);
        addCellWall(cells, BOTTOM, 1, 0, 1, 2, 3, 4, 5, 6, 7, 8);
        addCellWall(cells, RIGHT, 1, 6, 4);
        addCellWall(cells, BOTTOM, 2, 0, 1, 2);
        addCellWall(cells, RIGHT, 2, 2, 4, 5, 7, 8);
        addCellWall(cells, BOTTOM, 3, 3, 4, 6);
        addCellWall(cells, RIGHT, 3, 1, 2, 4, 7);
        addCellWall(cells, BOTTOM, 4, 1, 6);
        addCellWall(cells, RIGHT, 4, 0, 4, 5, 7, 8);
        addCellWall(cells, BOTTOM, 5, 1, 3, 4, 8);
        addCellWall(cells, RIGHT, 5, 1, 2, 7, 8);
        addCellWall(cells, BOTTOM, 6, 3,4,5,6,7,8);
        addCellWall(cells, RIGHT, 6,1,2,7,8);
        addCellWall(cells, BOTTOM, 7,3,4,8);
        addCellWall(cells, RIGHT, 7,1,2,4,5,7,8);
        addCellWall(cells, BOTTOM, 8,0,6);
        addCellWall(cells, RIGHT, 8, 1, 2, 4, 5, 7);
        addCellWall(cells, BOTTOM, 9, 2,3,5,7);
        addCellWall(cells, RIGHT, 9,1,2,7,8);
        addCellWall(cells, BOTTOM, 10,4);
        addCellWall(cells, RIGHT, 10,1,2,4,6);
        addCellWall(cells, BOTTOM, 11,0,2,3,4,5,6,7);
        addCellWall(cells, RIGHT, 11,1,2);
        addCellWall(cells, BOTTOM, 12, 3, 4, 5, 6);
        buildAutoWalls(cells);
        cells[0][5].removeWall(LEFT);
        cells[12][5].removeWall(RIGHT);
    }

    /**
     * @see Map#paintExceptionCellWalls(Graphics, Cell[][])
     * @param graphics
     * @param cells dimension array of the cells on the map
     */
    @Override
    protected void paintExceptionCellWalls(Graphics graphics, Cell[][] cells) {
        cells[0][5].paintExceptionWall(graphics, 0);
        cells[COLUMNS -1][5].paintExceptionWall(graphics, COLUMNS -1);
    }

    /**
     * @see Map#setActorsBase(Cell[][])
     * @param cells
     */
    @Override
    protected void setActorsBase(Cell[][] cells) {
        setEnemyBase(cells[6][4]);
        cells[6][4].removeWall(TOP);
        setPacmanBase(cells[6][6]);
    }

    /**
     * @see Map#placeSuperFoods()
     */
    @Override
    protected void placeSuperFoods() {
        placeSuperFood(0, 0);
        placeSuperFood(0, 8);
        placeSuperFood(12, 0);
        placeSuperFood(12, 8);
        placeSuperFood(5, 8);
        placeSuperFood(7, 8);
    }

    /**
     * @see Map#removeFoods()
     */
    @Override
    protected void removeFoods() {
        removeCellFood(0, 4, 6);
        removeCellFood(1, 4, 6);
        removeCellFood(3, 4, 7, 8);
        removeCellFood(4, 7, 8);
        removeCellFood(5, 4);
        removeCellFood(6, 4, 7, 8);
        removeCellFood(7, 4);
        removeCellFood(8, 7, 8);
        removeCellFood(9, 8);
        removeCellFood(11, 1 ,2, 4, 6);
        removeCellFood(12, 4, 6);
    }
}
